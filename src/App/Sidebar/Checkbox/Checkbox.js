import React from 'react'
import { connect } from 'react-redux'
import { setHideAssigned } from 'store/actions'

function Checkbox({ label, hideAssignedNames, dispatch }) {
  return (
    <label className="checkbox">
      <input
        type="checkbox"
        checked={hideAssignedNames}
        onChange={e => dispatch(setHideAssigned(e.target.checked))}
      />
      <span className="checkmark"></span>
      <span>{label}</span>
    </label>
  )
}

const mapStateToProps = ({ hideAssignedNames }) => ({ hideAssignedNames })

export default connect(mapStateToProps)(Checkbox)
