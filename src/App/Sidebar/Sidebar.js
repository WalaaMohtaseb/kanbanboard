import React from 'react'
import Item from 'App/Item/Item'
import SearchInput from './SearchInput/SearchInput'
import Checkbox from './Checkbox/Checkbox'
import { connect } from 'react-redux'
import { getNames } from 'store/selectors'

function Sidebar({ names }) {
  return (
    <div className="sidebar">
      <h2>Name List</h2>
      <SearchInput placeholder="Search" />
      <Checkbox label="Hide Assigned" />
      {names.length ? (
        <ul>
          {names.map(({ id, groups, title }) => (
            <Item
              id={id}
              key={id}
              title={title}
              groups={groups}
              from="sidebar"
            />
          ))}
        </ul>
      ) : (
        <p>No names to show!</p>
      )}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    names: getNames(state)
  }
}

export default connect(mapStateToProps)(Sidebar)
