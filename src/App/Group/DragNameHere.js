import React, { useRef } from 'react'
import { useDrop } from 'hooks/dnd'
import { addName, moveName } from 'store/actions'
import { connect } from 'react-redux'

function DragNameHere({ groupId, addName, moveName }) {
  const ref = useRef()

  const onEnter = () => ref.current.classList.add('over')
  const onLeave = () => ref.current.classList.remove('over')

  const onDrop = ({ id: nameId, from }) => {
    from === 'sidebar'
      ? addName({ groupId, nameId })
      : moveName({ from, to: { groupId }, nameId })
  }

  useDrop({ ref, onDrop, onEnter, onLeave })

  return (
    <div className="drag-here" ref={ref}>
      + Drag Name Here
    </div>
  )
}

export default connect(null, { addName, moveName })(DragNameHere)
