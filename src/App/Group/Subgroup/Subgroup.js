import React, { useState, useRef } from 'react'
import Item from 'App/Item/Item'
import Triangle from './Triangle/Triangle'
import { useDrop } from 'hooks/dnd'
import { addName, moveName, updateSubgroupTitle } from 'store/actions'
import { connect } from 'react-redux'
import { notify } from 'react-notify-toast'
import useInput from 'hooks/useInput'

function Subgroup({ id: subgroupId, title, names, id, groupId, dispatch }) {
  const ref = useRef()
  const titleInput = useInput(title)
  const [expanded, setExpanded] = useState(true)

  const onEnter = () => ref.current.classList.add('over')
  const onLeave = () => ref.current.classList.remove('over')

  const onDrop = ({ id: nameId, from }) => {
    from === 'sidebar'
      ? dispatch(addName({ groupId, subgroupId, nameId }))
      : dispatch(moveName({ from, to: { groupId, subgroupId }, nameId }))
  }

  useDrop({ ref, onDrop, onEnter, onLeave })

  function onTitleBlur() {
    if (titleInput.isDirty()) {
      dispatch(
        updateSubgroupTitle({
          groupId,
          subgroupId,
          title: titleInput.get()
        })
      )
      notify.show('Subgroup title updated!', 'success')
    }
  }

  return (
    <li className={`subgroup ${id}`} ref={ref}>
      <div>
        <input
          className="transparent title"
          type="text"
          ref={titleInput.ref}
          onBlur={onTitleBlur}
          spellCheck={false}
          placeholder="Subgroup title"
        />
        {expanded || <span>({names.length})</span>}
        <Triangle
          direction={expanded ? 'up' : 'down'}
          onClick={() => setExpanded(!expanded)}
        />
      </div>
      <ul style={{ display: expanded || 'none' }}>
        {names.map(({ title, id }) => (
          <Item key={id} id={id} title={title} from={{ groupId, subgroupId }} />
        ))}
      </ul>
    </li>
  )
}

export default connect()(Subgroup)
