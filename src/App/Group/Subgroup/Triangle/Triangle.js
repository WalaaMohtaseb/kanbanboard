import React from 'react'

export default function Triangle({ direction, onClick }) {
  return (
    <i className="triangle" onClick={onClick}>
      <div
        style={{
          transform: direction === 'down' && 'rotate(180deg)'
        }}
      ></div>
    </i>
  )
}
