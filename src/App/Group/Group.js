import React from 'react'
import Subgroup from './Subgroup/Subgroup'
import { lightenColor } from 'utils'
import { connect } from 'react-redux'
import { getSubgroups } from 'store/selectors'
import DragNameHere from './DragNameHere'
import { updateGroup } from 'store/actions'
import { notify } from 'react-notify-toast'
import useInput from 'hooks/useInput'
import LineTo from 'react-lineto';

function Group({ id, dispatch, title, color, subgroups, subtitles }) {
  const titleInput = useInput(title)
  const subtitleInput = useInput(subtitles[0] || '')
  console.log(subgroups, 'subgroups')
  const onBlur = e => {
    if (!e.relatedTarget && (titleInput.isDirty() || subtitleInput.isDirty())) {
      const [title, subtitle] = [titleInput.get(), subtitleInput.get()]
      dispatch(updateGroup({ id, title, subtitle }))
      notify.show('Group has been updated!', 'success')
    }
  }

  return (
    <li className="group">
      <div
        tabIndex="0"
        className="group-header"
        onBlur={onBlur}
        style={{
          background: `linear-gradient(
            90deg,
            ${lightenColor(color, 70)} 0%,
            ${color} 100%
          )`
        }}
      >
        <input
          type="text"
          className="transparent title"
          spellCheck={false}
          ref={titleInput.ref}
          placeholder="Group title"
        />

        <input
          type="text"
          className="transparent subtitle"
          ref={subtitleInput.ref}
          spellCheck={false}
          placeholder="Group subtitle"
        />
      </div>

      <ul>
        {subgroups.map((props, i) => (
          <>
          <Subgroup groupId={id} key={props.id} {...props} />
          {/* <LineTo from={props.id} to={props.linkTo} /> */}
          </>
        ))}
      </ul>
      <DragNameHere groupId={id} />
    </li>
  )
}

const mapStateToProps = (state, props) => {
  return {
    subgroups: getSubgroups(state, props.id)
  }
}

export default connect(mapStateToProps)(Group)
