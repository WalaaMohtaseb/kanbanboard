import React, { useState, useEffect } from 'react'
import './App.sass'
import Sidebar from './Sidebar/Sidebar'
import Group from './Group/Group'
import { connect } from 'react-redux'
import { loadInitialData, addNewGroup } from 'store/actions'

function App({ groups, dispatch }) {
  const [ready, setReady] = useState(false)

  useEffect(() => {
    dispatch(loadInitialData()).then(() => setReady(true))
  }, [])

  return (
    ready && (
      <>
        <Sidebar />

        <div id="content">
          <ul className="groups">
            {Object.keys(groups).map(id => (
              <Group key={id} {...groups[id]} />
            ))}

            <li className="group">
              <div
                onClick={() => dispatch(addNewGroup())}
                className="group-header add-new"
              >
                + Add New Title
              </div>
            </li>
          </ul>
        </div>
      </>
    )
  )
}

const mapStateToProps = ({ groups }) => ({ groups })

export default connect(mapStateToProps)(App)
