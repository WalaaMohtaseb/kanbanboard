import React, { useRef, useEffect } from 'react'
import GroupTag from './GroupTag/GroupTag'
import { useDrag } from 'hooks/dnd'
import { removeItem } from 'store/actions'
import { connect } from 'react-redux'

function Item({ id, title, groups, from, dispatch }) {
  const ref = useRef()
  const groupTags = useRef()

  useDrag({
    id,
    ref,
    from
  })

  useEffect(() => {
    if (groupTags.current) {
      groupTags.current.addEventListener('wheel', e => {
        e.deltaY > 0
          ? (groupTags.current.scrollLeft += 100)
          : (groupTags.current.scrollLeft -= 100)
      })
    }
  }, [])

  return (
    <li className="item" ref={ref}>
      <span>{title}</span>
      {from === 'sidebar' && (
        <ul className="group-tags" ref={groupTags}>
          {groups.map(({ id, ...props }) => (
            <GroupTag key={id} {...props} />
          ))}
        </ul>
      )}

      {from !== 'sidebar' && (
        <i
          onClick={() => dispatch(removeItem({ id, ...from }))}
          className="close"
        >
          &times;
        </i>
      )}
    </li>
  )
}

export default connect()(Item)
