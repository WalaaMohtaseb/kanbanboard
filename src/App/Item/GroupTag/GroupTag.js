import React from 'react'
import { lightenColor } from 'utils'

export default function GroupTag({ title, color }) {
  return (
    <li
      style={{
        background: `linear-gradient(
      90deg,
      ${lightenColor(color, 70)} 0%,
      ${color} 100%
    )`
      }}
    >
      {title}
    </li>
  )
}
