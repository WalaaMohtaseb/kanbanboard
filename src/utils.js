export function lightenColor(color, amount) {
  return (
    '#' +
    color
      .replace(/^#/, '')
      .replace(/../g, color =>
        (
          '0' +
          Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)
        ).substr(-2)
      )
  )
}

export function keyBy(collection, key) {
  const result = {}
  collection.forEach(item => {
    result[item[key]] = item
  })
  return result
}

export function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}

export const on = (el, events) => {
  for (let eventName in events)
    el.addEventListener(eventName, events[eventName])
}

export const off = (el, events) => {
  for (let eventName in events)
    el.removeEventListener(eventName, events[eventName])
}
