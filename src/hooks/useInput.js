import React, { useEffect, useRef } from 'react'

export default function useInput(defaultValue) {
  const ref = useRef()

  useEffect(() => {
    ref.current.value = defaultValue
  }, [])

  const get = () => ref.current.value
  const isDirty = () => get() !== defaultValue
  const save = () => (defaultValue = get())

  return { ref, get, isDirty, save }
}
