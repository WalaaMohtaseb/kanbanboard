import { useEffect } from 'react'
import { on, off } from 'utils'

let draggingState = null

export const useDrag = ({
  effect,
  ref,
  onDragStart,
  onDragOver,
  onDragEnd,
  ...other
}) => {
  const dragstart = e => {
    e.dataTransfer.dropEffect = effect
    draggingState = other
    onDragStart && onDragStart()
  }
  const dragover = () => onDragOver && onDragOver()
  const dragend = () => {
    onDragEnd && onDragEnd()
    draggingState = null
  }
  useEffect(() => {
    if (ref.current) {
      ref.current.setAttribute('draggable', true)
      on(ref.current, { dragstart, dragover, dragend })
      return () => off(ref.current, { dragstart, dragover, dragend })
    }
  }, [])
}

export const useDrop = ({ ref, onDrop, onEnter, onLeave }) => {
  let counter = 0
  const dragover = e => e.preventDefault()

  const drop = e => {
    e.preventDefault()
    onDrop(draggingState)
    onLeave()
  }

  const dragenter = () => ++counter === 1 && onEnter()
  const dragleave = () => --counter === 0 && onLeave()

  useEffect(() => {
    if (ref.current) {
      on(ref.current, { dragover, drop, dragenter, dragleave })
      return () => off(ref.current, { dragover, drop, dragenter, dragleave })
    }
  })
}
