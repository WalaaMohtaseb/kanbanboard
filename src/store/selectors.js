import { createSelector } from 'reselect'

export const getSubgroups = createSelector(
  state => state.names,
  (state, groupId) => state.groups[groupId].subgroups,
  (names, subgroups) => {
    return Object.keys(subgroups).map(id => {
      return {
        ...subgroups[id],
        names: Array.from(subgroups[id].names)
          .map(nameId => names[nameId])
          .filter(Boolean)
      }
    })
  }
)

export const getNames = createSelector(
  state => state.names,
  state => state.groups,
  state => state.hideAssignedNames,
  state => state.nameSearch,
  (names, groups, hideAssigned, nameSearch) => {
    return Object.keys(names)
      .map(nameId => {
        const isAssigned = !!names[nameId].groups.size

        if (hideAssigned && isAssigned) {
          return null
        }

        return {
          ...names[nameId],
          groups: Array.from(names[nameId].groups).map(
            groupId => groups[groupId]
          )
        }
      })
      .filter(name => {
        return name
          ? nameSearch
            ? name.title.match(new RegExp(nameSearch, 'i'))
            : true
          : false
      })
  }
)
