import {
  LOAD_INITIAL_DATA,
  ADD_NAME,
  MOVE_NAME,
  UPDATE_SUBGROUP_TITLE,
  ADD_NEW_GROUP,
  UPDATE_GROUP,
  SET_HIDE_ASSIGNED,
  UPDATE_NAME_SEARCH,
  REMOVE_ITEM
} from './types'
import { keyBy } from 'utils'
import { LIST_OF_COLORS } from '../constants'
import { uuid } from 'utils'

export const initialState = {
  names: {},
  groups: {},

  hideAssignedNames: false,
  nameSearch: ''
}

export const reducer = {
  [LOAD_INITIAL_DATA]: (state, { names, groups }) => {
    names.forEach(name => (name.groups = new Set([])))
    state.names = keyBy(names, 'id')

    groups.forEach((group, i) => {
      group.color = LIST_OF_COLORS[i % LIST_OF_COLORS.length]
      group.subgroups.forEach(subgroup => {
        subgroup.names = new Set(subgroup.names)
        subgroup.names.forEach(
          nameId =>
            state.names[nameId] && state.names[nameId].groups.add(group.id)
        )
      })
      group.subgroups = keyBy(group.subgroups, 'id')
    })

    state.groups = keyBy(groups, 'id')
  },

  [ADD_NAME]: (state, { groupId, subgroupId, nameId }) => {
    if (!state.groups[groupId].subgroups[subgroupId]) {
      subgroupId = uuid()
      state.groups[groupId].subgroups[subgroupId] = {
        id: subgroupId,
        title: '',
        names: new Set([]),
        linkTo: null
      }
    }

    state.groups[groupId].subgroups[subgroupId].names.add(nameId)
    state.names[nameId].groups.add(groupId)
  },

  [MOVE_NAME]: (state, { from, to, nameId }) => {
    if (!to.subgroupId) {
      to.subgroupId = uuid()
      state.groups[to.groupId].subgroups[to.subgroupId] = {
        id: to.subgroupId,
        title: '',
        names: new Set([]),
        linkTo: null
      }
    }

    state.names[nameId].groups.delete(from.groupId)
    state.names[nameId].groups.add(to.groupId)
    state.groups[from.groupId].subgroups[from.subgroupId].names.delete(nameId)
    state.groups[to.groupId].subgroups[to.subgroupId].names.add(nameId)
  },

  [UPDATE_SUBGROUP_TITLE]: (state, { groupId, subgroupId, title }) => {
    state.groups[groupId].subgroups[subgroupId].title = title
  },

  [ADD_NEW_GROUP]: state => {
    const id = uuid()
    state.groups[id] = {
      id,
      title: '',
      subtitles: [],
      subgroups: {},
      color:
        LIST_OF_COLORS[Object.keys(state.groups).length % LIST_OF_COLORS.length]
    }
  },

  [UPDATE_GROUP]: (state, { id, title, subtitle }) => {
    state.groups[id].title = title
    state.groups[id].subtitles = [subtitle]
  },

  [SET_HIDE_ASSIGNED]: (state, checked) => {
    state.hideAssignedNames = checked
  },

  [UPDATE_NAME_SEARCH]: (state, value) => {
    state.nameSearch = value
  },

  [REMOVE_ITEM]: (state, { id, groupId, subgroupId }) => {
    state.names[id].groups.delete(groupId)
    state.groups[groupId].subgroups[subgroupId].names.delete(id)
  }
}
