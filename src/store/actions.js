import {
  LOAD_INITIAL_DATA,
  ADD_NAME,
  MOVE_NAME,
  UPDATE_SUBGROUP_TITLE,
  ADD_NEW_GROUP,
  UPDATE_GROUP,
  SET_HIDE_ASSIGNED,
  UPDATE_NAME_SEARCH,
  REMOVE_ITEM
} from './types'
import api from 'api'

export const loadInitialData = () => async dispatch => {
  let [names, groups] = await Promise.all([
    api.getAllNames(),
    api.getAllGroups()
  ])

  return dispatch({
    type: LOAD_INITIAL_DATA,
    payload: { names, groups }
  })
}

export const addName = payload => {
  return {
    type: ADD_NAME,
    payload
  }
}

export const moveName = payload => {
  return {
    type: MOVE_NAME,
    payload
  }
}

export const updateSubgroupTitle = payload => {
  return {
    type: UPDATE_SUBGROUP_TITLE,
    payload
  }
}

export const addNewGroup = () => {
  return {
    type: ADD_NEW_GROUP
  }
}

export const updateGroup = payload => {
  return {
    type: UPDATE_GROUP,
    payload
  }
}

export const setHideAssigned = payload => {
  return {
    type: SET_HIDE_ASSIGNED,
    payload
  }
}

export const updateNameSearch = payload => {
  return {
    type: UPDATE_NAME_SEARCH,
    payload
  }
}

export const removeItem = payload => {
  return {
    type: REMOVE_ITEM,
    payload
  }
}
