import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { reducer, initialState } from './reducer'
import produce from 'immer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default createStore(
  (state = initialState, action) =>
    produce(state, draft => {
      if (action && action.type && reducer[action.type] instanceof Function) {
        reducer[action.type](draft, action.payload)
      }
    }),
  composeEnhancers(applyMiddleware(thunk))
)
