import { names, groups } from './mockData'

const api = {}

api.getAllNames = () =>
  new Promise(resolve => {
    resolve(names)
  })

api.getAllGroups = () =>
  new Promise(resolve => {
    resolve(groups)
  })

export default api
