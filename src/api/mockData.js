export const names = [
  {
    title: 'Name A',
    id: 'id001'
  },
  {
    title: 'Name B',
    id: 'id002'
  },
  {
    title: 'Name C',
    id: 'id003'
  },
  {
    title: 'Name D',
    id: 'id004'
  },
  {
    title: 'Name E',
    id: 'id005'
  }
]

export const groups = [
  {
    id: 'group-id-1',
    title: 'Group Title A',
    subtitles: ['Subtitle Aa', 'Subtitle Ab'],
    subgroups: [
      {
        title: 'Subgroup A',
        id: 'subgroupid001',
        names: ['id001', 'id002', 'id003', 'id004'],
        linkTo: 'subgroupid011'
      },
      {
        title: 'Subgroup B',
        id: 'subgroupid002',
        names: ['id005', 'id006'],
        linkTo: 'subgroupid012'
      },
      {
        title: 'Subgroup C',
        id: 'subgroupid003',
        names: ['id007'],
        linkTo: 'subgroupid013'
      },
      {
        title: 'Subgroup D',
        id: 'subgroupid004',
        names: ['id008', 'id009', 'id010'],
        linkTo: 'subgroupid014'
      }
    ]
  },
  {
    id: 'group-id-2',
    title: 'Group Title B',
    subtitles: ['Subtitle Baa', 'Subtitle Bbb', 'Subtitle Bcc'],
    subgroups: [
      {
        title: 'Subgroup AA',
        id: 'subgroupid011',
        names: ['id011', 'id004'],
        linkTo: null
      },
      {
        title: 'Subgroup AB',
        id: 'subgroupid012',
        names: ['id012'],
        linkTo: null
      },
      {
        title: 'Subgroup AC',
        id: 'subgroupid013',
        names: ['id013'],
        linkTo: null
      },
      {
        title: 'Subgroup AD',
        id: 'subgroupid014',
        names: ['id014', 'id015'],
        linkTo: null
      }
    ]
  },
  {
    id: 'group-id-3',
    title: 'Group Title C',
    subtitles: ['Subtitle Cc'],
    subgroups: [
      {
        title: 'Subgroup ABB',
        id: 'subgroupid021',
        names: ['id011', 'id012'],
        linkTo: null
      },
      {
        title: 'Subgroup ACC',
        id: 'subgroupid022',
        names: ['id025'],
        linkTo: null
      },
      {
        title: 'Subgroup ADD',
        id: 'subgroupid023',
        names: [],
        linkTo: null
      }
    ]
  }
]
