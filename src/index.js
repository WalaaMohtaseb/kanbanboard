import React from 'react'
import ReactDOM from 'react-dom'
import App from './App/App'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import store from './store'
import Notifications from 'react-notify-toast'

const rootEl = document.getElementById('root')

ReactDOM.render(
  <Provider store={store}>
    <Notifications options={{ timeout: 1000 }} />
    <App />
  </Provider>,
  document.getElementById('root')
)

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./App/App', () => {
    const NextApp = require('./App/App').default
    ReactDOM.render(<NextApp />, rootEl)
  })
}

serviceWorker.register()
